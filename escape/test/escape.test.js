const { expect } = require('chai');
const { escape, unescape } = require('../index');

describe('#escape', _ => {
  it('converts & into &amp;', () => {
    expect(escape('&')).to.be.equal('&amp;');
  });
  
  it('converts " into &quot;', () => {
    expect(escape('"')).to.be.equal('&quot;');
  });

  it('converts \' into &#39;', () => {
    expect(escape('\'')).to.be.equal('&#39;');
  });

  it('converts < into &lt;', () => {
    expect(escape('<')).to.be.equal('&lt;');
  });

  it('converts > into &gt;', () => {
    expect(escape('>')).to.be.equal('&gt;');
  });
});

describe('#unescape', _ => {
  it('converts &amp; into &', () => {
    expect(unescape('&amp;')).to.be.equal('&');
  });
  
  it('converts &quot; into "', () => {
    expect(unescape('&quot;')).to.be.equal('"');
  });

  it('converts &#39; into \'', () => {
    expect(unescape('&#39;')).to.be.equal('\'');
  });

  it('converts &lt; into <', () => {
    expect(unescape('&lt;')).to.be.equal('<');
  });

  it('converts &gt; into >', () => {
    expect(unescape('&gt;')).to.be.equal('>');
  });
});
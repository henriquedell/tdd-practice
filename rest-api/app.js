const express = require('express'),
bodyParser = require('body-parser'),
logger = require('morgan'),
mongodb = require('mongodb')

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(logger('dev'))

const client = new mongodb.MongoClient('mongodb://localhost:27017', { useUnifiedTopology: true })
let db;
client.connect(function(err) {
  if (err) {
    client.close()
    console.log(err)
  }

  db = client.db('test')
});

app.param('collectionName', (req, res, next, collectionName) => {
  req.collection = db.collection(collectionName)
  return next()
})

app.get('/', (req, res, next) => {
  res.send('please select a collection, e.g., /collections/messages')
})

app.get('/collections/:collectionName', (req, res, next) => {
  req.collection.find({}, { limit: 10, sort: { '_id': -1 } }).toArray((e, results) => {
    if (e) return next(e)
    res.send(results)
  })
})

app.post('/collections/:collectionName', (req, res, next) => {
  req.collection.insertOne(req.body, {}, function(e, results){
    if (e) return next(e)
    res.send(results.ops)
  })
})

app.get('/collections/:collectionName/:id', (req, res, next) => {
  req.collection.find({ _id: mongodb.ObjectId(req.params.id) }).toArray((e, result) => {
    if (e) return next(e)
    res.send(result[0])
  })
})

app.put('/collections/:collectionName/:id', (req, res, next) => {
  req.collection.updateOne({ _id: mongodb.ObjectId(req.params.id) }, { $set: req.body }, (e, results) => {
    if (e) return next(e)
    res.send((results.result.n === 1) ? {} : { message: 'error' })
  })
})

app.delete('/collections/:collectionName/:id', (req, res, next) => {
  req.collection.deleteOne({ _id: mongodb.ObjectId(req.params.id) }, (e, results) => {
    if (e) return next(e)
    res.send((results.result.n === 1) ? {} : { message: 'error' })
  })
})

if (require.main == module) {
  app.listen(3000, () => {
    console.log('Express server listening on port 3000')
  })
} else {
  module.exports = app
}
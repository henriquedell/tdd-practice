const supertest = require('supertest');
const { expect } = require('chai');
const app = require('../app');
const mongodb = require('mongodb');

describe('Express API Server', _ => {
  let id;
  let db;

  before(() => {
    const client = new mongodb.MongoClient('mongodb://localhost:27017', { useUnifiedTopology: true })
    client.connect(function(err) {
      if (err) {
        client.close()
        console.log(err)
      }

      db = client.db('test')
    });
  })

  it('Posts an object', async () => {
    const res = await supertest(app).post('/collections/test')
    .send({
      name: 'John',
      email: 'john@example.com'
    })
    .catch(console.log);
    expect(res.body).to.have.lengthOf(1);
    expect(res.body[0]._id.length).to.be.equal(24);
    id = res.body[0]._id;
  });

  it('Retrieves an object', () => {
    supertest(app).get(`/collections/test/${id}`)
    .end((err, res) => {
      expect(err).to.be.eql(null);
      expect(typeof res.body).to.be.eq('object');
      expect(res.body._id.length).to.be.eq(24);
      expect(res.body._id).to.be.equals(id);
      expect(res.body.name).to.be.equal('John');
    });
  });

  it('Retrieves a collection', () => {
    supertest(app).get('/collections/test')
    .end((err, res) => {
      expect(err).to.be.equal(null);
      expect(res.body.length).to.be.above(0);
      expect(res.body.map(item => item._id)).to.contain(id);
    });
  });

  it('Updates an object', async () => {
    const res = await supertest(app).put(`/collections/test/${id}`)
    .send({
      name: 'Henri'
    })
    .expect(200)
    .catch(console.log);
    expect(typeof res.body).to.be.eq('object');
    expect(res.body).to.be.deep.equal({});
  });

  it('Checks a recently updated object', () => {
    supertest(app).get(`/collections/test/${id}`)
    .end((err, res) => {
      expect(err).to.be.equal(null);
      expect(typeof res.body).to.be.eq('object');
      expect(res.body._id.length).to.be.eq(24);
      expect(res.body._id).to.be.equals(id);
      expect(res.body.name).to.be.equal('Henri');
    })
  });

  it('Removes an object', () => {
    supertest(app).delete(`/collections/test/${id}`)
    .expect(200)
    .end((err, res) => {
      expect(err).to.be.equal(null);
      expect(typeof res.body).to.be.eq('object');
      expect(res.body).to.be.deep.equal({});
    });
  });

  it('Checks for removed object', () => {
    supertest(app).get('/collections/test')
    .end((err, res) => {
      expect(err).to.be.equal(null);
      expect(res.body.length).to.be.above(0);
      expect(res.body.map(item => item._id)).to.not.contain(id);
    });
  });

  after(() => {
    db.dropCollection('test');
  });
});